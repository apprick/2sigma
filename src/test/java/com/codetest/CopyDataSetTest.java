package com.codetest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Have not written boundry cases since didn't have time
 * 
 * @author rsingh
 * 
 */
public class CopyDataSetTest {

	@Test
	public void testCopyDataSetInfo() {
		final int numOfDataCenter = 5;
		final List<String> dataCenterInfos = new ArrayList<String>();

		final String dataCenter1 = "4 1 2 4 5";
		dataCenterInfos.add(dataCenter1);
		final String dataCenter2 = "2 3 1";
		dataCenterInfos.add(dataCenter2);
		final String dataCenter3 = "3 2 4 1";
		dataCenterInfos.add(dataCenter3);
		final String dataCenter4 = "1 5";
		dataCenterInfos.add(dataCenter4);
		final String dataCenter5 = "3 2 1 3";
		dataCenterInfos.add(dataCenter5);

		final DataSetInfo dataSetInfo = new DataSetInfo(numOfDataCenter, dataCenterInfos);
		dataSetInfo.buildMissingDataInfo();

		Map<Integer, Set<Integer>> dataPresentAtMap = dataSetInfo.getDataPresentAtInfo();
		Map<Integer, Set<Integer>> dataMissingMap = dataSetInfo.getMissingDataIdInfo();

		System.out.println(dataMissingMap);
		System.out.println(dataPresentAtMap);

		final Set<Integer> missingAtNode1 = new HashSet<Integer>();
		missingAtNode1.add(3);
		Assert.assertEquals(missingAtNode1, dataMissingMap.get(1));

		final Set<Integer> missingAtNode2 = new HashSet<Integer>();
		missingAtNode2.add(2);
		missingAtNode2.add(4);
		missingAtNode2.add(5);
		Assert.assertEquals(missingAtNode2, dataMissingMap.get(2));

		final Set<Integer> missingAtNode3 = new HashSet<Integer>();
		missingAtNode3.add(3);
		missingAtNode3.add(5);
		Assert.assertEquals(missingAtNode3, dataMissingMap.get(3));

		final Set<Integer> missingAtNode4 = new HashSet<Integer>();
		missingAtNode4.add(1);
		missingAtNode4.add(3);
		missingAtNode4.add(2);
		missingAtNode4.add(4);
		Assert.assertEquals(missingAtNode4, dataMissingMap.get(4));

		final Set<Integer> missingAtNode5 = new HashSet<Integer>();
		missingAtNode5.add(4);
		missingAtNode5.add(5);
		Assert.assertEquals(missingAtNode5, dataMissingMap.get(5));

		// test for data present at
		final Set<Integer> dataPresentAtNode1 = new HashSet<Integer>();
		dataPresentAtNode1.add(1);
		dataPresentAtNode1.add(2);
		dataPresentAtNode1.add(3);
		dataPresentAtNode1.add(5);
		Assert.assertEquals(dataPresentAtNode1, dataPresentAtMap.get(1));

		final Set<Integer> dataPresentAtNode2 = new HashSet<Integer>();
		dataPresentAtNode2.add(1);
		dataPresentAtNode2.add(3);
		dataPresentAtNode2.add(5);
		Assert.assertEquals(dataPresentAtNode2, dataPresentAtMap.get(2));

		final Set<Integer> dataPresentAtNode3 = new HashSet<Integer>();
		dataPresentAtNode3.add(2);
		dataPresentAtNode3.add(5);
		Assert.assertEquals(dataPresentAtNode3, dataPresentAtMap.get(3));

		final Set<Integer> dataPresentAtNode4 = new HashSet<Integer>();
		dataPresentAtNode4.add(1);
		dataPresentAtNode4.add(3);
		Assert.assertEquals(dataPresentAtNode4, dataPresentAtMap.get(4));

		final Set<Integer> dataPresentAtNode5 = new HashSet<Integer>();
		dataPresentAtNode5.add(1);
		dataPresentAtNode5.add(4);
		Assert.assertEquals(dataPresentAtNode5, dataPresentAtMap.get(5));

		dataSetInfo.printCopyInstructions();
	}
}
