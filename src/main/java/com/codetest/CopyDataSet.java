package com.codetest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CopyDataSet {

	public static void main(String[] args) throws IOException {
		System.out.println("Please enter number of data center : ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String data = br.readLine();
		final int numOfDataCenters = Integer.parseInt(data);

		final List<String> dataCenterInfos = new ArrayList<String>();
		for (int i = 0; i < numOfDataCenters; i++) {
			System.out.println("enter data set id info");
			br = new BufferedReader(new InputStreamReader(System.in));
			dataCenterInfos.add(data = br.readLine());
		}

		final DataSetInfo dataSetInfo = new DataSetInfo(numOfDataCenters, dataCenterInfos);
		dataSetInfo.buildMissingDataInfo();
		dataSetInfo.printCopyInstructions();
	}

}
