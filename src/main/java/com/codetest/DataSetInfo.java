package com.codetest;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * The algorithm is to build the info for each data center and for each data set
 * id The input is parsed and a map of what data set id is missing on each node
 * is built. Along with that we also build a particular data set is present on
 * what all data centers.
 * 
 * With the help of these two maps the problem becomes simple of going through
 * the first map and finding out where to look for the missing data set
 * 
 * Have assumed that input will be always correct so have not added validations
 * 
 * @author rsingh
 * 
 */
public class DataSetInfo {
	private final int numOfDataCenters;
	private final List<String> dataCenterInfos;

	/**
	 * represents what all data set ids are missing for each data center
	 */
	private final Map<Integer, Set<Integer>> missingDataIdInfo = new HashMap<Integer, Set<Integer>>();

	/**
	 * Map representing on what all datacenters a particular dataset is present
	 */
	private final Map<Integer, Set<Integer>> dataPresentAtInfo = new HashMap<Integer, Set<Integer>>();

	public DataSetInfo(final int numOfDataCenters, final List<String> dataCenterInfos) {
		this.numOfDataCenters = numOfDataCenters;
		this.dataCenterInfos = dataCenterInfos;
	}

	public void buildMissingDataInfo() {
		for (int dataCenterId = 1; dataCenterId <= this.dataCenterInfos.size(); dataCenterId++) {
			final String[] tokens = this.dataCenterInfos.get(dataCenterId - 1).split(" ");
			final int numOfIdsPresent = Integer.parseInt(tokens[0]);

			Set<Integer> allDataSetIds = new HashSet<Integer>();
			for (int i = 0; i < this.numOfDataCenters; i++)
				allDataSetIds.add(i + 1);

			for (int i = 1; i <= numOfIdsPresent; i++) {
				final int dataSetId = Integer.parseInt(tokens[i]);
				allDataSetIds.remove(dataSetId);
				addDataPresentInfo(dataSetId, dataCenterId);
			}

			// build missing data map
			for (Integer missingDataSet : allDataSetIds)
				addMissingDataForDataCenter(dataCenterId, missingDataSet);
		}
	}

	private void addMissingDataForDataCenter(int dataCenterId, int missingDataSetId) {
		Set<Integer> missingDataAtNode = this.missingDataIdInfo.get(dataCenterId);

		if (missingDataAtNode == null) {
			missingDataAtNode = new HashSet<Integer>();
			this.missingDataIdInfo.put(dataCenterId, missingDataAtNode);
		}

		missingDataAtNode.add(missingDataSetId);
	}

	private void addDataPresentInfo(int dataSetId, int dataCenterId) {

		Set<Integer> dataSetIdsPresent = this.dataPresentAtInfo.get(dataSetId);

		if (dataSetIdsPresent == null) {
			dataSetIdsPresent = new HashSet<Integer>();
			this.dataPresentAtInfo.put(dataSetId, dataSetIdsPresent);
		}

		dataSetIdsPresent.add(dataCenterId);
	}

	public void printCopyInstructions() {
		for (Entry<Integer, Set<Integer>> entry : this.missingDataIdInfo.entrySet()) {
			final int dataCenterId = entry.getKey();
			final Set<Integer> missingDataSets = entry.getValue();

			for (Integer missingData : missingDataSets) {
				System.out.println(missingData + " " + this.dataPresentAtInfo.get(missingData).iterator().next() + " " + dataCenterId);
			}
		}
	}

	// used in test
	Map<Integer, Set<Integer>> getMissingDataIdInfo() {
		return Collections.unmodifiableMap(missingDataIdInfo);
	}

	// used in test
	Map<Integer, Set<Integer>> getDataPresentAtInfo() {
		return Collections.unmodifiableMap(dataPresentAtInfo);
	}
}
