package com.codetest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class TwoSigmaCodeTestJumbleSort {

	public static List<String> sortedOutPut(final List<String> input) {
		if (input == null)
			return null;

		final List<String> sortedOutput = new ArrayList<String>();
		for (String line : input) {
			sortedOutput.add(sortJumbleString(line));
		}

		return sortedOutput;
	}

	private static String sortJumbleString(String line) {
		String[] tokens = line.split(" ");

		final Set<String> sortedString = new TreeSet<String>();
		final Set<Integer> sortedIntegers = new TreeSet<Integer>();

		for (String token : tokens) {
			if (isInteger(token)) {
				sortedIntegers.add(Integer.parseInt(token));
			} else {
				sortedString.add(token);
			}
		}

		final StringBuilder outputBuilder = new StringBuilder();

		int i;
		Iterator<String> sortedStringIter;
		Iterator<Integer> sortedIntegerIter;
		for (i = 0, sortedStringIter = sortedString.iterator(), sortedIntegerIter = sortedIntegers.iterator(); i < tokens.length
				&& (sortedStringIter.hasNext() || sortedIntegerIter.hasNext()); i++) {
			if (isInteger(tokens[i])) {
				outputBuilder.append(sortedIntegerIter.next());
			} else {
				outputBuilder.append(sortedStringIter.next());
			}

			if (i != tokens.length - 1)
				outputBuilder.append(" ");
		}

		return outputBuilder.toString();
	}

	private static boolean isInteger(String in) {
		try {
			Integer.parseInt(in);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
}
